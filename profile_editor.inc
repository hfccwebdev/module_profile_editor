<?php
// $Id: $

/**
 * @file
 * Included code for Profile Editor
 */

/**
 * Page callback for profile_editor.
 */
function profile_editor_edit($form_state, $account) {
  drupal_set_title(t('Profile Editor for') . ' ' . check_plain($account->name));
  $form = array();
  $form['#attributes']['enctype'] = 'multipart/form-data';
  $form['_account'] = array('#type' => 'value', '#value' => $account);
  $form['_category'] = array('#type' => 'value', '#value' => $category);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'), '#weight' => 90);
  $form['#uid'] = $account->uid; // array('#type' => 'hidden', '#value' => $account->uid);

  if (user_access('enable and disable users')) {
    $form['Account Control'] = array(
      '#type' => 'fieldset',
      '#title' => t('Account Control'),
      '#weight' => -10,
      'status' => array(
        '#type' => 'radios',
        '#title' => t('User account status'),
        '#default_value' => $account->status,
        '#options' => array(t('Blocked'), t('Active')),
      ),
    );
  }

  $edit = (empty($form_state['values'])) ? (array)$account : $form_state['values'];
  $categories = profile_categories();
  uasort($categories, 'element_sort');
  foreach ($categories as $category) {
    $name = $category['name'];
    $fields= profile_form_profile($edit, $account, $category['name']);
    $form[$name] = $fields[$name];
  }

  // Picture/avatar:
  if (variable_get('user_pictures', 0) && !$register) {
    $form['picture'] = array('#type' => 'fieldset', '#title' => t('Picture'), '#weight' => 80);
    $picture = theme('user_picture', (object) $edit);
    if ($edit['picture']) {
      $form['picture']['current_picture'] = array('#value' => $picture);
      $form['picture']['picture_delete'] = array('#type' => 'checkbox', '#title' => t('Delete picture'), '#description' => t('Check this box to delete your current picture.'));
    }
    else {
      $form['picture']['picture_delete'] = array('#type' => 'hidden');
    }
    $form['picture']['picture_upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload picture'),
      '#size' => 48,
      '#description' => t('Your virtual face or picture. Maximum dimensions are %dimensions and the maximum size is %size kB.', array('%dimensions' => variable_get('user_picture_dimensions', '85x85'), '%size' => variable_get('user_picture_file_size', '30'))) . ' ' . variable_get('user_picture_guidelines', '')
    );
    $form['#validate'][] = 'user_profile_form_validate';
    $form['#validate'][] = 'user_validate_picture';
  }

  // Displays debug information about the form.
  $Xform['formdump'] = array(
    '#type' => 'markup',
    '#title' => t('Raw form dump'),
    '#value' => '<pre>' . print_r($form, 1) . '</pre>',
    '#weight' => 98,
  );

  // Displays debug information about the user.
  $Xform['userdump'] = array(
    '#type' => 'markup',
    '#title' => t('Raw User Data'),
    '#value' => '<pre>' . print_r($account, 1) . '</pre>',
    '#weight' => 99,
  );

  return $form;
}

/**
 * Validate form sumbissions.
 *
 * I think this needs to be invoked to fire off photo upload.
 */
function profile_editor_edit_validate($form, &$form_state) {
}

/**
 * Process form submissions.
 */
function profile_editor_edit_submit($form, &$form_state) {
  $uid = $form['#uid'];

  $account = $form_state['values']['_account'];
  unset($form_state['values']['_account'], $form_state['values']['op'], $form_state['values']['submit'], $form_state['values']['delete'], $form_state['values']['form_token'], $form_state['values']['form_id'], $form_state['values']['_category']);
  $edit = $form_state['values'];

  //drupal_set_message('<pre>' . print_r($form_state['values'], 1) . '</pre>');

  _user_edit_submit($uid, $edit);
  user_save($account, $edit);

  $categories = profile_categories();
  uasort($categories, 'element_sort');
  foreach ($categories as $category) {
    $name = $category['name'];
    profile_save_profile($edit, $account, $name);
  }

  cache_clear_all();
  
  drupal_set_message(t('Finished processing updates for user ') . $form['#uid']);
}


/**
 * Implementation of hook_form_alter().
 */
function profile_editor_form_alter(&$form, &$form_state, $form_id) {
  switch($form_id) {
    case 'profile_editor_edit':
      $form['Personal Information']['#weight'] = 2;
      break;
  }
}
